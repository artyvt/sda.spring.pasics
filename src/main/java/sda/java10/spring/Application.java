package sda.java10.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Starting application, Now.");
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        CampaignService campaignService = context.getBean(CampaignService.class);
        campaignService.sendCampaignLetter();
    }
}
