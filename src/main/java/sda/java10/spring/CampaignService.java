package sda.java10.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class CampaignService {

    private final ClientService clientService;
    private final EmailService emailService;

    public CampaignService(ClientService clientService, EmailService emailService) {
        this.clientService = clientService;
        this.emailService = emailService;
    }


    public void sendCampaignLetter() {
        List<String> allClientEmails = clientService.getAllClientEmails();

        for (String email :
                allClientEmails) {
            emailService.sendEmail(email, "Super awesome potatoes deal!");
        }

    }
}
