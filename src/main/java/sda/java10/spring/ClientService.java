package sda.java10.spring;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class ClientService {

    public List<String> getAllClientEmails() {
        return Arrays.asList("email_1", "email_2");
    }
}
