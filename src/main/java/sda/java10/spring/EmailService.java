package sda.java10.spring;

public interface EmailService {

    void sendEmail(String email, String message);

}
