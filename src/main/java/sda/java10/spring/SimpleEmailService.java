package sda.java10.spring;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class SimpleEmailService implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleEmailService.class);

    @Override
    public void sendEmail(String email, String message) {
        LOGGER.info("Sending message to: " + email);
    }


}
